INTRODUCTION
------------

This module adds new field type, polish REGON identification number.
It supports 9-digit and 14-digit long REGON numbers.
REGON (from Rejestr Gospodarki Narodowej - Register of the National Economy)
is a unique number granted to businesses in Poland.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

1. Unzip the files to the "sites/all/modules" directory and enable the module.
2. Create a new node type and add new field REGON.

CONFIGURATION
-------------

No special configuration needed.
